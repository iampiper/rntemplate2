import React, { Component } from 'react'
import { Text, View } from 'react-native'
import RootNavigator from 'src/router'
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import {store,persistor} from 'src/redux/store'

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootNavigator />
        </PersistGate>
      </Provider>
    )
  }
}

