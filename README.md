# 1 ] Add firebase initialization code for ios and android 
    
* First npm install && react-native link
* Rename app name in index.js 

* For ios
    1. Add GoogleService-Info.plist
    2. Add below code in AppDelegate.m
        * `#import <Firebase.h>`
        * `[FIRApp configure];`

    3. Add pods 
        * platform :ios, '9.0'
        * pod 'Firebase/Core', '~> 5.15.0'
        * pod 'Firebase/AdMob', '~> 5.15.0'  
        * pod 'Firebase/Database', '~> 5.15.0'

* For android 
    1. Add google-service.json 
    
    2. Add below code in project gradle 
        * classpath 'com.google.gms:google-services:4.2.0'

    3. Add below code in app gradle 
        * apply plugin: 'com.google.gms.google-services'

    4. add dependencies in app build gradle 
        * implementation "com.google.android.gms:play-services-base:16.1.0"
        * implementation "com.google.firebase:firebase-core:16.0.8"
        * implementation "com.google.firebase:firebase-database:16.1.0"
        * implementation "com.google.firebase:firebase-ads:15.0.1"

    5. Add packages in MainApplication
        * import io.invertase.firebase.database.RNFirebaseDatabasePackage;
            * new RNFirebaseDatabasePackage()
        * import io.invertase.firebase.admob.RNFirebaseAdMobPackage; 
            * new RNFirebaseAdMobPackage()
        * import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
            * new RNFirebaseAnalyticsPackage()     

    6. add proguard rules (optional)
        * -keep class io.invertase.firebase.** { *; }
        * -dontwarn io.invertase.firebase.**
        



