import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './style'

import Icon from 'react-native-vector-icons/FontAwesome';

class Home extends Component {

  static navigationOptions = ({ navigation }) => {
    let config = {
      title: 'Demo Screen'
    }
    return config
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <View style={[styles.container]}>
        <Text> Demo </Text>
      </View>
    );
  }
}

export default Home;
