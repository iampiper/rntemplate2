import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { NavigationActions,StackActions } from 'react-navigation'

class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.resetToAuth = StackActions.reset(
            {
                index: 0,
                key: null,
                actions: [
                    NavigationActions.navigate({ routeName: 'Auth' })
                ]
            }
        )

        setTimeout(() => {
            this.props.navigation.dispatch(this.resetToAuth)
        }, 2000);
    }

    render() {
        return (
            <View style={[styles.container]}>
                <Text> Splash </Text>
            </View>
        );
    }
}

export default Splash;
