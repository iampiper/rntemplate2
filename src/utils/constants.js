import Colors from "./colors";

export default Constants = {
    DEFAULT_HEADER: {
        headerStyle: {
            backgroundColor: Colors.HEADER_BG
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    },
    // test ids 
    banner:"",
    reward:""
}