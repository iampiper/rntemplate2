export default Colors = {
    COLOR_PRIMARY: '#2196F3',
    COLOR_SECONDERY : '#1976D2',
    COLOR_TURNERY : '#EF5350',
    HEADER_BG : '#2196F3',
    DRAWER_TOP_COLOR:'#000000',
    WHITE:'#ffffff',
    BLACK:'#000000',
    DRAWER_ITEM_BG:'#F7F7F7'
}