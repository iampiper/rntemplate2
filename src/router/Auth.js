import { createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import Home from 'src/screens/Auth/Home'
import Demo from 'src/screens/Auth/Demo'
import Constants from "src/utils/constants";
import Routes from './routes'

const AuthStack = createStackNavigator(
    {
        [Routes.Home]: Home,
        [Routes.Demo]: Demo,
    },
    {
        defaultNavigationOptions: Constants.DEFAULT_HEADER,
        headerLayoutPreset: 'center'
    }

);
export default (AuthStack);