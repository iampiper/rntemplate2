import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import Colors from 'src/utils/colors'
import Icon from 'react-native-vector-icons/FontAwesome';
import Share from 'react-native-share'
import LinearGradient from 'react-native-linear-gradient';
import Routes from './routes'

export default class Drawer extends Component {

    navigateToScreen = (route) => (
        () => {
            const navigateAction = NavigationActions.navigate({
                routeName: route
            });
            this.props.navigation.dispatch(navigateAction);
        })

    _share = () => {
        let options = {
            url: "",
            title: "React Native",
            message: "Hey install this app.",
            subject: "Share Link"
        }
        Share.open(options)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

    render() {
        let route = this.props.navigation.state.routes[0]
        console.log(route)
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <View style={{ flex: 1, width: 280, justifyContent: 'center', backgroundColor: Colors.DRAWER_TOP_COLOR }} >

                    </View>
                </View>
                <View style={styles.screenContainer}>
                    <TouchableOpacity style={styles.screenStyle} onPress={this.navigateToScreen(Routes.Demo)}>
                        <Text style={[styles.screenTextStyle]}>Demo </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.screenStyle} onPress={this._share}>
                        <Text style={[styles.screenTextStyle]}>Share </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.screenStyle} onPress={this.navigateToScreen(Routes.Demo)}>
                        <Text style={[styles.screenTextStyle]}>Rate Us</Text>
                    </TouchableOpacity>
                </View>
                
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
        // alignItems: 'center',
    },
    headerContainer: {
        height: 150,
    },
    headerText: {
        color: '#fff8f8',
    },
    screenContainer: {
        flex: 1,

    },
    screenStyle: {
        flexDirection: 'row',
        height: 60,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        backgroundColor: Colors.DRAWER_ITEM_BG,
        borderBottomWidth: 1,
        borderColor: Colors.BLACK,
    },
    screenTextStyle: {
        fontSize: 20,
        color: Colors.BLACK,

    },
    linearGradient:{
        
    }

});