import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import Home from 'src/screens/Auth/Home'
import Splash from 'src/screens/Splash'
import Constants from "src/utils";
import Drawer from './drawer'
import AuthStack from './Auth'

const DrawerNavigator = createDrawerNavigator(
    {
        AuthStack: AuthStack
    },
    {
        contentComponent: Drawer,
    }

);

const RootNavigator = createStackNavigator(
    {
        Splash: {
            screen: Splash
        },
        Auth: DrawerNavigator
    },
    {   
        defaultNavigationOptions:{
            header:null
        }
    }

);

export default createAppContainer(RootNavigator);