import {
    SET_TOKEN,
    SET_USER,
} from "./action-types";

import {REHYDRATE} from 'redux-persist/src/constants';

let initialState = {
    user: null,
    token: null
};

const setToken = (token) => ({type: SET_TOKEN, token});
const setUser = (user) => ({type: SET_USER, user});
const logout = () => ({type: REHYDRATE, payload: initialState});

export {setToken, setUser, logout};